var Game = function()
{
  this.canvas = document.getElementById('game-canvas'),
  this.context = this.canvas.getContext('2d'),
  this.fpsElement = document.getElementById('fps'),
  this.toastElement = document.getElementById('toast'),
  this.instructionElement = document.getElementById('instructions'),
  this.copyrightElement = document.getElementById('copyright'),
  this.highScoreElement = document.getElementById("highscore"),
  this.soundAndMusicElement = document.getElementById('sound-and-music'),
  this.loadingElement = document.getElementById('loading'),
  this.loadingTitleElement = document.getElementById('loading-title'),

  //Time
  this.playing = true,
  this.timeSystem = new TimeSystem(),
  this.timeRate = 1.0, //1.0 is normal time, 0.5 1/2 speed

  //Pixels and meters
  this.CANVAS_WIDTH_IN_METRES = 13,
  this.PIXELS_PER_METRE = this.canvas.width/this.CANVAS_WIDTH_IN_METRES,

  //Gravity
  this.GRAVITY_FORCE = 9.81,

  //Mobile
   this.mobileInstructionsVisible = false,
   this.mobileStartToast = document.getElementById('start-toast'),
   this.mobileWelcomeToast = document.getElementById('mobile-welcome-toast'),
   this.welcomeStartLink = document.getElementById('welcome-start-link'),
   this.showHowLink = document.getElementById('show-how-link'),
   this.mobileStartLink = document.getElementById('mobile-start-link'),

  //Constants
  this.RIGHT = 1,
  this.LEFT = 2,
  this.BUTTON_PACE_VELOCITY = 80,
  this.RUNNER_LEFT = 50,
  this.PLATFORM_HEIGHT = 8,
  this.PLATFORM_STROKE_WIDTH = 2,

  this.RUNNER_EXPOSION_DURATION = 500,
  this.BAD_GUYS_EXPLOSION_DURATION = 1500,

  this.STARTING_RUNNER_TRACK = 1,

  this.TRACK_1_BASELINE = 323,
  this.TRACK_2_BASELINE = 223,
  this.TRACK_3_BASELINE = 123,

  this.SHORT_DELAY = 50,
  this.OPAQUE = 1.0,
  this.TRANSPARENT = 0,

  //Game states
  this.paused = false,
  this.PAUSED_CHECK_INTERVAL = 200, //time in milliseconds
  this.pauseStartTime,
  this.windowHasFocus = true,
  this.countDownInProgress = false,
  this.gameStarted = false,

  //Images
  this.background = new Image(),
  this.spritesheet = new Image(),
  this.life = new Image();

  //Time
  this.lastAnimationFrameTime = 0,
  this.lastFpsUpdateTime = 0,
  this.fps = 60;

 //Warning for low fps
  this.warning_fps = 0;

  //level
  this.currentLevel = 1;
  this.currentTimeRate = 1;
  this.INITIAL_PLATFORM_NUM = 10;
  this.INITIAL_PLATFORM_GAP = 220;
  this.INITIAL_ROCK_SPAWN_RATE = 20;
  this.INITIAL_COIN_SPAWN_RATE = 80;
  this.RUBY_SPAWN_RATE = 5;
  this.SAPPHIRE_SPAWN_RATE = 5;
  
  this.currentPlatformNum = this.INITIAL_PLATFORM_NUM;
  this.currentPlatformGap = this.INITIAL_PLATFORM_GAP;
  this.rockSpawnRate = this.INITIAL_ROCK_SPAWN_RATE;
  this.coinSpawnRate = this.INITIAL_COIN_SPAWN_RATE;

  //score
  this.currentScore = 0;
  this.scoreBeforeThisLevel = 0;
  this.highScoreThisSession = 0;

  this.playerLife = 3;

  //Music
  this.musicElement = document.getElementById('music'),
  this.musicElement.volume = 0.1,
  this.musicCheckboxElement = document.getElementById('music-checkbox'),
  this.soundCheckboxElement = document.getElementById('sound-checkbox'),
  this.musicOn = this.musicCheckboxElement.checked,
  this.audioSprites= document.getElementById('audio-sprites'),
  this.soundOn = this.soundCheckboxElement.checked,

  this.audioChannels = [
      {playing: false, audio: this.audioSprites},
      {playing: false, audio: null,},
      {playing: false, audio: null,},
      {playing: false, audio: null}],

  this.audioSpriteCountdown = this.audioChannels.length - 1,
  this.graphicsReady = false,

   this.coinSound = 
   {
      position: 7.1,
      duration: 588,
      volume: 0.5
   },

   this.electricityFlowingSound = 
   {
      position: 1.03,
      duration: 1753,
      volume: 0.5
   },

   this.explosionSound = 
   {
      position: 4.3,
      duration: 760,
      volume: 1.0
   },

   this.pianoSound = 
   {
      position: 5.6,
      duration: 395,
      volume: 0.5
   },

   this.thudSound = 
   {
      position: 3.1,
      duration: 809,
      volume: 1.0
   },

  //Scrolling
  this.STARTING_BACKGROUND_OFFSET = 0,
  this.STARTING_BACKGROUND_VELOCITY = 0,
  this.BACKGROUND_VELOCITY = 25,
  this.STARTING_PLATFORM_OFFSET = 0,
  this.PLATFORM_VELOCITY_MULTIPLER = 4.35,
  this.STARTING_SPRITE_OFFSET = 0,

  //Animation
  this.RUN_ANIMATION_RATE = 14,

  //Translation offsets
  this.backgroundOffset = this.STARTING_BACKGROUND_OFFSET,
  this.spriteOffset = this.STARTING_SPRITE_OFFSET,
  this.bgVelocity = this.STARTING_BACKGROUND_VELOCITY,
  this.platformOffset = this.STARTING_PLATFORM_OFFSET,
  this.platformVelocity;

  //Key codes
  this.KEY_D = 68,
  this.KEY_LEFT = 37,
  this.KEY_K = 75,
  this.KEY_RIGHT = 39,
  this.KEY_P = 80,

 // Sprite sheet cells................................................

   this.RUNNER_CELLS_WIDTH = 50; // pixels
   this.RUNNER_CELLS_HEIGHT = 57;

   this.COIN_CELLS_HEIGHT = 30;
   this.COIN_CELLS_WIDTH  = 30;
   
   this.EXPLOSION_CELLS_HEIGHT = 62;

   this.RUBY_CELLS_HEIGHT = 30;
   this.RUBY_CELLS_WIDTH = 35;

   this.SAPPHIRE_CELLS_HEIGHT = 30;
   this.SAPPHIRE_CELLS_WIDTH  = 35;

   this.ROCK_HEIGHT = 54;
   this.ROCK_WIDTH = 50;

   this.PLATFORM_1_HEIGHT = 35;
   this.PLATFORM_1_WIDTH = 80;

   this.PLATFORM_2_HEIGHT = 55;
   this.PLATFORM_2_WIDTH = 180;

   this.PLATFORM_3_HEIGHT = 60;
   this.PLATFORM_3_WIDTH = 70;

   this.PLATFORM_4_HEIGHT = 65;
   this.PLATFORM_4_WIDTH = 270;

   this.PLATFORM_5_HEIGHT = 40;
   this.PLATFORM_5_WIDTH = 120;

   this.explosionCells = 
   [
      { left: 20,   top: 20, 
        width: 52, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 81,  top: 20, 
        width: 70, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 160, top: 20, 
        width: 70, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 233, top: 20, 
        width: 70, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 325, top: 20, 
        width: 70, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 407, top: 20, 
        width: 70, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 490, top: 20, 
        width: 70, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 565, top: 20, 
        width: 70, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 655, top: 20, 
        width: 65, height: this.EXPLOSION_CELLS_HEIGHT }
   ],

   this.rubyCells = [
      { left: 15,   top: 109, width: this.RUBY_CELLS_WIDTH,
                             height: this.RUBY_CELLS_HEIGHT },

      { left: 51,  top: 109, width: this.RUBY_CELLS_WIDTH, 
                             height: this.RUBY_CELLS_HEIGHT },

      { left: 88,  top: 109, width: this.RUBY_CELLS_WIDTH, 
                             height: this.RUBY_CELLS_HEIGHT },

      { left: 124, top: 109, width: this.RUBY_CELLS_WIDTH, 
                             height: this.RUBY_CELLS_HEIGHT },

      { left: 161, top: 109, width: this.RUBY_CELLS_WIDTH, 
                             height: this.RUBY_CELLS_HEIGHT }
   ];

   this.sapphireCells = [
      { left: 199,   top: 109, width: this.SAPPHIRE_CELLS_WIDTH,
                             height: this.SAPPHIRE_CELLS_HEIGHT },

      { left: 235,   top: 109, width: this.SAPPHIRE_CELLS_WIDTH,
                             height: this.SAPPHIRE_CELLS_HEIGHT },

      { left: 272,   top: 109, width: this.SAPPHIRE_CELLS_WIDTH,
                             height: this.SAPPHIRE_CELLS_HEIGHT },

      { left: 308,   top: 109, width: this.SAPPHIRE_CELLS_WIDTH,
                             height: this.SAPPHIRE_CELLS_HEIGHT },

      { left: 345,   top: 109, width: this.SAPPHIRE_CELLS_WIDTH,
                             height: this.SAPPHIRE_CELLS_HEIGHT }
   ];

   this.runnerCellsLeft = 
   [
      { left: 568, top: 173, 
        width: 51, height: 57 },

      { left: 531, top: 173, 
         width: 34, height: 57 },

      { left: 523, top: 172, 
         width: 39, height: 58 },

      { left: 416, top: 172, 
         width: 54, height: 58 },

      { left: 352, top: 172, 
         width: 58, height: 55 },

      { left: 298, top: 174, 
         width: 44, height: 56 },

      { left: 251,  top: 173, 
         width: 37, height: 57 },

      { left: 202,  top: 173, 
         width: 42, height: 57 },

      { left: 141,   top: 173, 
         width: 49, height: 57 },
      
      { left: 69,  top: 174, 
         width: 57, height: 56 },
      
      { left: 8,  top: 174, 
         width: 49, height: 56 }
   ],

   this.runnerCellsRight = 
   [
      { left: 10,  top: 249, 
         width: 51, height: 57 },

      { left: 64,  top: 249, 
         width: 34, height: 57 },

      { left: 106,   top: 248, 
         width: 39, height: 58 }, 
         
      { left: 159,  top: 248, 
        width: 54, height: 58 },
         
      { left: 220,  top: 248, 
        width: 58, height: 55 },

      { left: 287, top: 250, 
         width: 44, height: 56 },

      { left: 341, top: 249, 
         width: 37, height: 57 },

      { left: 385, top: 249, 
         width: 42, height: 57 },

      { left: 439, top: 249, 
         width: 49, height: 57 },
         
      { left: 503, top: 250, 
        width: 57, height: 56 },            

      { left: 572, top: 250, 
        width: 49, height: 56 }
   ],

   this.winCells = 
   [
      { left: 349,  top: 355, 
         width: 41, height: 68 },

      { left: 406,  top: 355, 
         width: 42, height: 68 },

      { left: 462,   top: 351, 
         width: 43, height: 72 }, 
         
      { left: 520,  top: 355, 
        width: 42, height: 68 },
         
      { left: 577,  top: 355, 
        width: 47, height: 68 }
   ],

   this.blueCoinCells = 
   [
      { left: 16,   top: 333, 
        width: this.COIN_CELLS_WIDTH,
        height: this.COIN_CELLS_HEIGHT },

      { left: 46,  top: 333, 
        width: this.COIN_CELLS_WIDTH, 
        height: this.COIN_CELLS_HEIGHT },
   ],

   this.goldCoinCells = 
   [
      { left: 78, top: 333, width: this.COIN_CELLS_WIDTH, 
                            height: this.COIN_CELLS_HEIGHT },
      { left: 108, top: 333, width: this.COIN_CELLS_WIDTH, 
                            height: this.COIN_CELLS_HEIGHT },
      { left: 141, top: 333, width: this.COIN_CELLS_WIDTH, 
                             height: this.COIN_CELLS_HEIGHT },
   ],

   this.rockCell =
   [
      {
        left: 18, top: 380, width: this.ROCK_WIDTH, 
                            height: this.ROCK_HEIGHT
      }
   ],

   this.portalCell =
   [
      {
        left: 84, top: 376, width: 47, height: 67
      }
   ],
  
  this.platformCell_2 =
  [
      {
        left: 133, top: 466, width: this.PLATFORM_2_WIDTH, 
                            height: this.PLATFORM_2_HEIGHT
      }
  ],

/*
  this.platformCell_1 =
  [
      {
        left: 38, top: 472, width: this.PLATFORM_1_WIDTH, 
                            height: this.PLATFORM_1_HEIGHT
      }
  ],
  
  this.platformCell_3 =
  [
      {
        left: 338, top: 466, width: this.PLATFORM_3_WIDTH, 
                            height: this.PLATFORM_3_HEIGHT
      }
  ],

  this.platformCell_4 =
  [
      {
        left: 27, top: 539, width: this.PLATFORM_4_WIDTH, 
                            height: this.PLATFORM_4_HEIGHT
      }
  ],

  this.platformCell_5 =
  [
      {
        left: 300, top: 543, width: this.PLATFORM_5_WIDTH, 
                            height: this.PLATFORM_5_HEIGHT
      }
  ];
*/

  // Randomly generated sprite data

  this.randomPlatformData = [ 
    {
      left: 10,
      width: this.TEMPWIDTH,
      height: this.PLATFORM_HEIGHT,
      opacity: 1.0,
      track: 1
    }
  ];

  this.randomCoinData = [];
  this.randomRockData = [];
  this.randomRubiesData = [];
  this.randomSapphiresData = [];
  this.randomPortalData = [];

  this.TEMPWIDTH = 171;

   //Life icon position
   this.lifeData = [
      { left: 60, top: 10 }, 
      { left: 85, top: 10 }, 
      { left: 110, top: 10 }, 
   ];


  //Sprite Behaviours
    this.runBehaviour = 
    {
      lastAdvanceTime: 0,

      execute: function(sprite, now, fps, context, lastAnimationFrameTime)
      {
        if(sprite.runAnimationRate === 0)
        {
          return;
        }
        if(this.lastAdvanceTime === 0)
        {
          this.lastAdvanceTime = now;
        }
        else if(now - this.lastAdvanceTime > 1000/sprite.runAnimationRate)
        {
          sprite.artist.advance();
          this.lastAdvanceTime = now;
        }
      }
    };

    this.paceBehaviour = {
      setDirection: function(sprite)
      {
        var sRight = sprite.left + sprite.width,
            pRight = sprite.platform.left + sprite.platform.width;

        if(sprite.direction === undefined)
        {
          sprite.direction = game.RIGHT;
        }
        if(sRight > pRight && sprite.direction === game.RIGHT)
        {
          sprite.direction = game.LEFT;
        }
        else if(sprite.left < sprite.platform.left && sprite.direction === game.LEFT)
        {
          sprite.direction = game.RIGHT;
        }
      },

      setPosition: function(sprite, now, lastAnimationFrameTime)
      {
        //console.log(sprite.velocityX);
        var pixelsToMove = sprite.velocityX * (now - lastAnimationFrameTime)/1000;
        //console.log(pixelsToMove);

        if(sprite.direction === game.RIGHT)
        {
          sprite.left += pixelsToMove;
        }
        else
        {
          sprite.left -= pixelsToMove;
        }
      },

      execute: function(sprite, now, fps, context, lastAnimationFrameTime)
      {
        //console.log("Pace Behaviour Execute being called");
        this.setDirection(sprite);
        this.setPosition(sprite, now, lastAnimationFrameTime);
      }
    };

    //Runner Jump Behaviour
    this.jumpBehaviour = 
    {
      pause: function(sprite, now)
      {
        if(sprite.ascendTimer.isRunning())
        {
          sprite.ascendTimer.pause(now);
        }
        else if(sprite.descendTimer.isRunning())
        {
          sprite.descendTimer.pause(now);
        }
      },

      unpause: function(sprite, now)
      {
        if(sprite.ascendTimer.isRunning())
        {
          sprite.ascendTimer.unpause(now);
        }
        else if(sprite.descendTimer.isRunning())
        {
          sprite.descendTimer.unpause(now);
        }
      },

      isAscending: function(sprite)
      {
        return sprite.ascendTimer.isRunning();
      },

      ascend: function(sprite, now)
      {
        var elapsedTime = sprite.ascendTimer.getElapsedTime(now),
            deltaY = elapsedTime/(sprite.JUMP_DURATION/2) * sprite.JUMP_HEIGHT;
        sprite.top = sprite.verticalLaunchPosition - deltaY;
      },

      isDoneAscending: function(sprite, now)
      {
        return sprite.ascendTimer.getElapsedTime(now) > sprite.JUMP_DURATION/2;
      },

      finishAscent: function(sprite, now)
      {
        sprite.jumpApex = sprite.top;
        sprite.ascendTimer.stop(now);
        sprite.descendTimer.start(now);
      },

      isDescending: function(sprite)
      {
        return sprite.descendTimer.isRunning();
      },

      descend: function(sprite, now)
      {
        var elapsedTime = sprite.descendTimer.getElapsedTime(now),
            deltaY = elapsedTime/(sprite.JUMP_DURATION/2) * sprite.JUMP_HEIGHT;
        sprite.top = sprite.jumpApex + deltaY;
      },

      isDoneDescending: function(sprite, now)
      {
        return sprite.descendTimer.getElapsedTime(now) > sprite.JUMP_DURATION/2;
      },

      finishDescent: function(sprite, now)
      {
        sprite.stopJumping();
        sprite.runAnimationRate = game.RUN_ANIMATION_RATE;
        if(game.platformUnderneath(sprite))
        {
          sprite.top = sprite.verticalLaunchPosition;
        }
        else
        {
          sprite.fall(game.GRAVITY_FORCE *
               (sprite.descendTimer.getElapsedTime(now)/1000) *
               game.PIXELS_PER_METER);
        }
        //sprite.runAnimationRate = game.RUN_ANIMATION_RATE;
      },

      execute: function(sprite, now, fps, context, lastAnimationFrameTime)
      {
        if(!sprite.jumping || sprite.falling)
        {
          return;
        }
        if(this.isAscending(sprite))
        {
          if(!this.isDoneAscending(sprite, now))
          {
            this.ascend(sprite, now);
          }
          else
          {
            this.finishAscent(sprite, now);
          }
        }
         else if(this.isDescending(sprite))
         {
          if(!this.isDoneDescending(sprite, now))
          {
            this.descend(sprite, now);
          }
          else
          {
            this.finishDescent(sprite, now);
          }
         }
      }
   };

    this.collideBehaviour=
    {
      //Three step process
      //1. What should we consider for collision?
      //2. Is there a collision
      //3. React to the collision
      isCandidateForCollision: function(sprite, otherSprite)
      {
        var s, o;
        s = sprite.calculateCollisionRectangle(),
        o = otherSprite.calculateCollisionRectangle();

        candidate =  o.left < s.right && sprite !== otherSprite &&
        sprite.visible && otherSprite.visible && !sprite.exploding && !otherSprite.exploding;
        return candidate;
      },

      didCollide: function(sprite, otherSprite, context)
      {
        var o = otherSprite.calculateCollisionRectangle(),
            r = sprite.calculateCollisionRectangle();
        //Determine if any of the four corners of the runner's
        //sprite or the centre lie within the other sprites
        //bounding box
        /*
        context.beginPath();
        context.rect(o.left, o.top, o.right - o.left, o.bottom - o.top);
        var collision = context.isPointInPath(r.left, r.top) || context.isPointInPath(r.right, r.top) || context.isPointInPath(r.left, r.bottom) || context.isPointInPath(r.right, r.bottom) || context.isPointInPath(r.centreX, r.centreY);
        return collision;
        */
        return this.didRunnerCollideWithOtherSprite(r, o, context);
      },
      
      didRunnerCollideWithOtherSprite: function (r, o, context) {
         // Determine if either of the runner's four corners or its
         // center lie within the other sprite's bounding box.

         context.beginPath();
         context.rect(o.left, o.top, o.right - o.left, o.bottom - o.top);

         return context.isPointInPath(r.left,  r.top)       ||
                context.isPointInPath(r.right, r.top)       ||

                context.isPointInPath(r.centerX, r.centerY) ||

                context.isPointInPath(r.left,  r.bottom)    ||
                context.isPointInPath(r.right, r.bottom);
      },


      processPlatformCollisionDuringJump: function(sprite, platform)
      {
        var isDescending = sprite.descendTimer.isRunning();
        sprite.stopJumping();
        if(isDescending)
        {
          sprite.track = platform.track;
          sprite.top = game.calculatePlatformTop(sprite.track) - sprite.height;
        }
        else
        {
          sprite.fall();
        }
      },

      processBadGuyCollision: function(sprite)
      {
        game.explode(sprite);
        game.shake();
        game.loseLife();
      },

      processPlatformCollisionDuringJump: function(sprite, platform)
      {
         var isDescending = sprite.descendTimer.isRunning();

         sprite.stopJumping();

         if (isDescending) { 
            sprite.track = platform.track;
            sprite.top = game.calculatePlatformTop(sprite.track) - 
                         sprite.height;
         }
         else { // Collided with platform while ascending
            sprite.fall();
         }
      },

      processCollision: function(sprite, otherSprite)
      {
        //console.log(sprite.type, otherSprite.type);
        if(sprite.jumping && 'platform' === otherSprite.type)
        {
          //console.log("Jump platform collision");
          this.processPlatformCollisionDuringJump(sprite, otherSprite);
        }
        else if('coin' === otherSprite.type)
        {
          game.addScore(100, game.currentScore);
          otherSprite.visible = false;
        }
        else if('portal' === otherSprite.type)
        {
          game.revealToast('Level Passed', 1500);
          otherSprite.visible = false;
          game.levelProceed();
        }
        else if('ruby' === otherSprite.type)
        {
          game.revealToast('Surprise MDFK', 1500);
          game.setTimeRate(game.currentTimeRate * 2);
          game.shake();
          otherSprite.visible = false;
          setTimeout(function(){
            game.setTimeRate(game.currentTimeRate);
          }, 5000);
        }
        else if('sapphire' === otherSprite.type)
        {
          game.setTimeRate(game.currentTimeRate / 2);
          otherSprite.visible = false;
          setTimeout(function(){
            game.setTimeRate(game.currentTimeRate);
          }, 5000);
        }
        else if('rock' === otherSprite.type)
        {
          this.processBadGuyCollision(sprite);
        }
      },

      execute: function(sprite, now, fps, context, lastAnimationFrameTime)
      {
         var otherSprite;
         for(var i=0; i < game.sprites.length; ++i)
         {
            otherSprite = game.sprites[i];
            if(this.isCandidateForCollision(sprite, otherSprite))
            {
               if(this.didCollide(sprite, otherSprite, context))
               {
                  this.processCollision(sprite, otherSprite);
               }
            }
         }
      }
   };

    //Fall Behaviour
    this.fallBehaviour = {

      pause: function(sprite, now)
      {
        sprite.fallTimer.pause(now);
      },

      unpause: function(sprite, now)
      {
        sprite.fallTimer.unpause(now);
      },

      isOutOfPlay: function(sprite)
      {
        return sprite.top > game.canvas.height;
      },

      setSpriteVelocity: function(sprite, now)
      {
        sprite.velocityY = sprite.initialVelocityY + game.GRAVITY_FORCE*(sprite.fallTimer.getElapsedTime(now)/1000)*game.PIXELS_PER_METRE;
      },

      calculateVerticalDrop: function(sprite, now, lastAnimationFrameTime)
      {
        return sprite.velocityY * (now-lastAnimationFrameTime)/1000;
      },

      willFallBelowCurrentTrack: function(sprite, dropDistance)
      {
        return sprite.top+sprite.height+dropDistance > game.calculatePlatformTop(sprite.track);
      },

      fallOnPlatform: function(sprite) 
      {
         sprite.stopFalling();
         game.putSpriteOnTrack(sprite, sprite.track);
      },

      moveDown: function(sprite, now, lastAnimationFrameTime)
      {
        var dropDistance;
        this.setSpriteVelocity(sprite, now);
        dropDistance = this.calculateVerticalDrop(sprite, now, lastAnimationFrameTime);
        if(!this.willFallBelowCurrentTrack(sprite, dropDistance))
        {
          sprite.top += dropDistance;
        }
        else
        {
          if(game.platformUnderneath(sprite))
          {
            this.fallOnPlatform(sprite);
            sprite.stopFalling();
          }
          else
          {
            sprite.track--;
            sprite.top += dropDistance;
          }
        }
      },
      /*
      execute: function(sprite, now, fps, context, lastAnimationFrameTime)
      {
        if(!sprite.falling)
        {
          if(!sprite.jumping && !game.platformUnderneath(sprite))
          {
            sprite.fall();
          }
        }
        else
        {
          if(this.isOutOfPlay(sprite) || sprite.exploding)
          {
            sprite.stopFalling();
            if(this.isOutOfPlay(sprite))
            {
              sprite.top = 0;
              sprite.left = -1000;
              game.loseLife();
            }
          }
          else
          {
            this.moveDown(sprite, now, lastAnimationFrameTime);
          }
        }
      }
      */
      execute: function(sprite, now, fps, context, lastAnimationFrameTime)
      {
        if (sprite.falling) 
        {
            if (! this.isOutOfPlay(sprite) && !sprite.exploding) 
            {
               this.moveDown(sprite, now, lastAnimationFrameTime);
            }
            else 
            { // Out of play or exploding               
               sprite.stopFalling();

               if (this.isOutOfPlay(sprite)) 
               {
                  game.loseLife();
                  //game.playSound(game.electricityFlowingSound);
                  game.runner.visible = false;
               }
            }
         }
         else 
         { // Not falling
            if (!sprite.jumping && !game.platformUnderneath(sprite)) 
            {
               sprite.fall();
            }
         }
      }
    };

    //Runner explosions

    this.runnerExplodeBehaviour = new CellSwitchBehaviour(
      this.explosionCells, this.RUNNER_EXPLOSION_DURATION, function (sprite, now, fps){return sprite.exploding;}, function(sprite, animator){ sprite.exploding = false;});

    //Detonate buttons
    this.blueButtonDetonateBehaviour = {
      execute: function(sprite, now, fps, lastAnimationFrameTime)
      {
        var BUTTON_REBOUND_DELAY = 10000,
            SECOND_BEE_EXPLOSION_DELAY = 700;
        if(!sprite.detonating)
        {
          return;
        }
        sprite.artist.cellIndex = 1;
        game.explode(game.bees[5]);
        setTimeout(function(){
          game.explode(game.bees[6]);
          game.setTimeRate(0.1);
        }, SECOND_BEE_EXPLOSION_DELAY);
        sprite.detonating = false;
        setTimeout(function(){
          sprite.artist.cellIndex = 0;
        }, BUTTON_REBOUND_DELAY);
        this.setTimeRate(this.currentTimeRate);
      }
    };

  //Sprites
  //This costs more memory but is faster to iterate through all of the sprites of a particular type
  this.sprites = [];
  this.coins = [];
  this.platforms = [];
  this.rocks = [];
  this.rubies = [];
  this.sapphires = [];
  this.portals = [];
};

//------------------------------------------------------------------

Game.prototype = 
{

  generateSpritePosition: function()
  {
    //starting platform, FIXED
    this.randomPlatformData = [ 
      {
        left: 10,
        width: this.TEMPWIDTH,
        height: this.PLATFORM_HEIGHT,
        opacity: 1.0,
        track: 1
      },
    ];

    this.randomCoinData = [];
    this.randomRockData = [];
    this.randomRubiesData = [];
    this.randomSapphiresData = [];
    this.randomPortalData = [];

    // ---Generate platform---
    for(var i = 1; i < this.currentPlatformNum; i++)
    {
      var left, width, height, opacity, track;
      this.left = this.randomPlatformData[i-1].left + this.currentPlatformGap;
      this.width = this.TEMPWIDTH;
      this.height = this.PLATFORM_HEIGHT;
      this.opacity = 1.0;

      var j = Math.random() * 100;
      if(j < 50)
      {
        if(this.randomPlatformData[i-1].track !== 1)
        {
          if(this.randomPlatformData[i-1].track === 3)
          {
            var k = Math.random() * 100;
            if(k > 50)
            {
              this.track = this.randomPlatformData[i-1].track - 2;
            }
            else
            {
              this.track = this.randomPlatformData[i-1].track - 1;
            }
          }
          else
          {
            this.track = this.randomPlatformData[i-1].track - 1;
          }
        }
        else
        {
          this.track = this.randomPlatformData[i-1].track + 1;
        }
      }
      else
      {
        if(this.randomPlatformData[i-1].track !== 3)
        {
          this.track = this.randomPlatformData[i-1].track + 1;
        }
        else
        {
          this.track = this.randomPlatformData[i-1].track - 1;
        }
      }

      var generatedPlatform = 
      {
        left: this.left,
        width: this.width,
        height: this.height,
        opacity: this.opacity,
        track: this.track
      };

      this.randomPlatformData.push(generatedPlatform);

      // ---Generate coins on platform---
      var coinLeft, coinTop;
      var l = Math.random() * 100;

      if(l < this.coinSpawnRate && i !== this.currentPlatformNum - 1)
      {
        var randPos = Math.random();
        this.coinLeft = generatedPlatform.left + generatedPlatform.width * randPos;
        this.coinTop = game.calculatePlatformTop(generatedPlatform.track) - this.COIN_CELLS_HEIGHT;

        var generatedCoin =
        {
          left: this.coinLeft,
          top: this.coinTop
        };

        this.randomCoinData.push(generatedCoin);
      }

      // ---Generate rock after the platform---
      var rockLeft, rockTop;
      var m = Math.random() * 100;
      if(m < this.rockSpawnRate && i !== this.currentPlatformNum - 1)
      {
        this.rockLeft = generatedPlatform.left + generatedPlatform.width;
        this.rockTop = 50;

        var generatedRock = 
        {
          left: this.rockLeft,
          top: this.rockTop
        };

        this.randomRockData.push(generatedRock);
      }

      // ---Generate ruby on the platform
      var rubyLeft, rubyTop;
      var n = Math.random() * 100;
      if(m < this.RUBY_SPAWN_RATE && i !== this.currentPlatformNum - 1)
      {
        var randPos_1 = Math.random();
        this.rubyLeft = generatedPlatform.left + generatedPlatform.width * randPos_1;
        this.rubyTop = game.calculatePlatformTop(generatedPlatform.track) - this.RUBY_CELLS_HEIGHT;

        var generatedRuby = 
        {
          left: this.rubyLeft,
          top: this.rubyTop
        };
        this.randomRubiesData.push(generatedRuby);
      }

      // ---Generate sapphire on the platform
      var sapphireLeft, sapphireTop;
      var o = Math.random() * 100;
      if(o < this.SAPPHIRE_SPAWN_RATE && i !== this.currentPlatformNum - 1)
      {
        var randPos_2 = Math.random();
        this.sapphireLeft = generatedPlatform.left + generatedPlatform.width * randPos_2;
        this.sapphireTop = game.calculatePlatformTop(generatedPlatform.track) - this.SAPPHIRE_CELLS_HEIGHT;

        var generatedSapphire = 
        {
          left: this.sapphireLeft,
          top: this.sapphireTop
        };
        this.randomSapphiresData.push(generatedSapphire);
      }

      // ---Generate portal at the last platform
      var portalLeft, portalTop;
      if(i == this.currentPlatformNum - 1)
      {
        this.portalLeft = generatedPlatform.left + generatedPlatform.width * 0.8;
        this.portalTop = game.calculatePlatformTop(generatedPlatform.track) - 67;
      
        var generatedPortal = 
        {
          left: this.portalLeft,
          top: this.portalTop
        };

        this.randomPortalData.push(generatedPortal);
      }
    }
  },

  createSprites: function()
  {
    this.generateSpritePosition();
    this.createPlatformSprites();
    this.createCoinSprites();
    this.createRunnerSprite();
    this.createRubySprites();
    this.createSapphireSprites();
    this.createRockSprites();
    this.createPortalSprites();
    this.initializeSprites();

    //Add all of the sprites to a single array
    this.addSpritesToSpriteArray();
  },

  resetSprites: function()
  {
    this.sprites = [];
    this.coins = [];
    this.platforms = [];
    this.rocks = [];
    this.rubies = [];
    this.sapphires = [];
    this.portals = [];

    this.randomPlatformData = [ 
      {
        left: 10,
        width: this.TEMPWIDTH,
        height: this.PLATFORM_HEIGHT,
        opacity: 1.0,
        track: 1
      }
    ];

    this.randomCoinData = [];
    this.randomRockData = [];
    this.randomPortalData = [];
    this.randomSapphiresData = [];
    this.randomRubiesData = [];
  },

  addSpritesToSpriteArray: function()
  {
    for(var i =0; i < this.platforms.length; ++i)
    {
      this.sprites.push(this.platforms[i]);
    }

    for(var i =0; i < this.coins.length; ++i)
    {
      this.sprites.push(this.coins[i]);
    }

    for(var i =0; i < this.rocks.length; ++i)
    {
      this.sprites.push(this.rocks[i]);
    }

    for(var i =0; i < this.rubies.length; ++i)
    {
      this.sprites.push(this.rubies[i]);
    }

    for(var i =0; i < this.sapphires.length; ++i)
    {
      this.sprites.push(this.sapphires[i]);
    }

    for(var i =0; i < this.portals.length; ++i)
    {
      this.sprites.push(this.portals[i]);
    }

    this.sprites.push(this.runner);
  },

  initializeSprites: function()
  {
    this.positionSprites(this.coins, this.randomCoinData);
    this.positionSprites(this.rocks, this.randomRockData);
    this.positionSprites(this.rubies, this.randomRubiesData);
    this.positionSprites(this.sapphires, this.randomSapphiresData);
    this.positionSprites(this.portals, this.randomPortalData);

    this.equipRunner();
  },

  equipRunner: function()
  {
    this.equipRunnerForJumping();
    this.equipRunnerForFalling();
  },

  equipRunnerForJumping: function()
  {
    var INITIAL_TRACK = 1,
        RUNNER_JUMP_HEIGHT = 120,
        RUNNER_JUMP_DURATION = 1000,
        EASING_FACTOR = 1.1;

    this.runner.JUMP_HEIGHT = RUNNER_JUMP_HEIGHT;
    this.runner.JUMP_DURATION = RUNNER_JUMP_DURATION;
    this.runner.jumping = false;
    this.runner.track = INITIAL_TRACK;
    this.runner.ascendTimer = new AnimationTimer(this.runner.JUMP_DURATION/2, AnimationTimer.makeEaseOutEasingFunction(EASING_FACTOR));
    this.runner.descendTimer = new AnimationTimer(this.runner.JUMP_DURATION/2, AnimationTimer.makeEaseInEasingFunction(EASING_FACTOR));

    this.runner.jump = function()
    {
      if(this.jumping)
      {
        return;
      }
      this.jumping = true;
      this.runAnimationRate = 0;
      this.verticalLaunchPosition = this.top;
      this.ascendTimer.start(game.timeSystem.calculateGameTime());
    };

    this.runner.stopJumping = function()
    {
      this.descendTimer.stop();
      this.jumping = false;
    }; 
  },

  equipRunnerForFalling: function()
  {
    this.runner.fallTimer = new AnimationTimer();
    this.runner.falling = false;
    this.runner.fall = function(initialVelocity)
    {
      this.falling = true;
      this.velocityY = initialVelocity || 0;
      this.initialVelocityY = initialVelocity || 0;
      this.fallTimer.start(game.timeSystem.calculateGameTime());
    };
    this.runner.stopFalling = function()
    {
      this.falling = false;
      this.velocityY = 0;
      this.fallTimer.stop(game.timeSystem.calculateGameTime());
    };
  },

  positionSprites: function(sprites, spriteData)
  {
    var sprite;
    for(var i=0; i<sprites.length; ++i)
    {
      sprite = sprites[i];
      if(spriteData[i].platformIndex)
      {
        this.putSpriteOnPlatform(sprite, this.platforms[spriteData[i].platformIndex]);
      }
      else
      {
        sprite.top = spriteData[i].top;
        sprite.left = spriteData[i].left;
      }
    }
  },

  putSpriteOnPlatform: function(sprite, platformSprite)
  {
    sprite.top = platformSprite.top - sprite.height;
    sprite.left = platformSprite.left;
    sprite.platform = platformSprite;
  },

  putSpriteOnTrack: function(sprite, track)
  {
    sprite.track = track;
    sprite.top = this.calculatePlatformTop(sprite.track) - sprite.height;
  },

  platformUnderneath: function(sprite, track)
  {
    var platform, platformUnderneath,
        sr = sprite.calculateCollisionRectangle(),
        pr;
    if(track === undefined)
    {
      track = sprite.track;
    }
    for(var i=0; i<game.platforms.length; ++i)
    {
      platform = game.platforms[i];
      pr = platform.calculateCollisionRectangle();
      if(track === platform.track)
      {
        if(sr.right > pr.left && sr.left < pr.right)
        {
          platformUnderneath = platform;
          break;
        }
      }
    }
    return platformUnderneath;
  },

  createCoinSprites: function()
  {
    var coin,
        COIN_SPARKLE_DURATION = 300,
        COIN_SPARKLE_INTERVAL = 600,
        BOUNCE_DURATION_BASE = 800,
        BOUNCE_HEIGHT_BASE = 50;
    for(var i=0; i<this.randomCoinData.length;++i)
    {
      if(i%2 === 0)
      {
        coin = new Sprite('coin', new SpriteSheetArtist(this.spritesheet, this.goldCoinCells), [new CycleBehaviour(COIN_SPARKLE_DURATION, COIN_SPARKLE_INTERVAL), new BounceBehaviour(BOUNCE_DURATION_BASE + BOUNCE_DURATION_BASE*Math.random(), BOUNCE_HEIGHT_BASE + BOUNCE_HEIGHT_BASE*Math.random())]);
      }
      else
      {
        coin = new Sprite('coin', new SpriteSheetArtist(this.spritesheet, this.blueCoinCells), [new CycleBehaviour(COIN_SPARKLE_DURATION, COIN_SPARKLE_INTERVAL), new BounceBehaviour(BOUNCE_DURATION_BASE + BOUNCE_DURATION_BASE*Math.random(), BOUNCE_HEIGHT_BASE + BOUNCE_HEIGHT_BASE*Math.random())]);
      }
      coin.width = this.COIN_CELLS_WIDTH;
      coin.height = this.COIN_CELLS_HEIGHT;
      coin.value = 50;
      this.coins.push(coin);
    }
  },

  createPlatformSprites: function()
  {
    var sprite, pd,
        PULSE_DURATION = 800,
        PULSE_OPACITY_THRESHOLD = 0.1;
    for(var i=0; i<this.randomPlatformData.length;++i)
    {
      pd = this.randomPlatformData[i];
      sprite = new Sprite('platform', /*this.platformArtist*/ new SpriteSheetArtist(this.spritesheet, this.platformCell_2));
      sprite.left = pd.left;
      sprite.width = pd.width;
      sprite.height = pd.height;
      //sprite.fillStyle = pd.fillStyle;
      sprite.opacity = pd.opacity;
      sprite.track = pd.track;
      sprite.button = pd.button;

      sprite.top = this.calculatePlatformTop(pd.track);
      this.platforms.push(sprite);
    }
  },

  createSapphireSprites: function()
  {
    var SPARKLE_DURATION = 100,
          BOUNCE_DURATION_BASE = 1000, // milliseconds
          BOUNCE_HEIGHT_BASE = 100,   // pixels
          sapphire,
          sapphireArtist = new SpriteSheetArtist(this.spritesheet, this.sapphireCells);
   
      for(var i = 0; i < this.randomSapphiresData.length; ++i) 
      {
         sapphire = new Sprite('sapphire', sapphireArtist, [ new CycleBehaviour(SPARKLE_DURATION), new BounceBehaviour(BOUNCE_DURATION_BASE + BOUNCE_DURATION_BASE * Math.random(), BOUNCE_HEIGHT_BASE +BOUNCE_HEIGHT_BASE * Math.random()) ]);
         sapphire.width = this.SAPPHIRE_CELLS_WIDTH;
         sapphire.height = this.SAPPHIRE_CELLS_HEIGHT;
         sapphire.value = 200;
         
         this.sapphires.push(sapphire);
      }
  },

  createPortalSprites: function()
  {
    var portal;

    for(var i=0; i<this.randomPortalData.length;++i)
    {
      portal = new Sprite('portal', new SpriteSheetArtist(this.spritesheet, this.portalCell));

      portal.width = 47;
      portal.height = 67;
      this.portals.push(portal);
    }
  },
  
  createRockSprites: function()
  {
    var rock;

    for(var i=0; i<this.randomRockData.length;++i)
    {
      rock = new Sprite('rock', new SpriteSheetArtist(this.spritesheet, this.rockCell), [new RockFallBehaviour()]);

      rock.width = this.ROCK_WIDTH;
      rock.height = this.ROCK_HEIGHT;
      this.rocks.push(rock);
    }
  },

  createRubySprites: function()
  {
    var SPARKLE_DURATION = 100,
          BOUNCE_DURATION_BASE = 1000, // milliseconds
          BOUNCE_HEIGHT_BASE = 100,   // pixels
          ruby,
          rubyArtist = new SpriteSheetArtist(this.spritesheet, this.rubyCells);
   
      for(var i = 0; i < this.randomRubiesData.length; ++i) 
      {
         ruby = new Sprite('ruby', rubyArtist, [ new CycleBehaviour(SPARKLE_DURATION), new BounceBehaviour(BOUNCE_DURATION_BASE + BOUNCE_DURATION_BASE * Math.random(), BOUNCE_HEIGHT_BASE +BOUNCE_HEIGHT_BASE * Math.random()) ]);
         ruby.width = this.RUBY_CELLS_WIDTH;
         ruby.height = this.RUBY_CELLS_HEIGHT;
         ruby.value = 200;
         
         this.rubies.push(ruby);
      }
  },

  createSapphireSprites: function()
  {
    var SPARKLE_DURATION = 100,
          BOUNCE_DURATION_BASE = 1000, // milliseconds
          BOUNCE_HEIGHT_BASE = 100,   // pixels
          sapphire,
          sapphireArtist = new SpriteSheetArtist(this.spritesheet, this.sapphireCells);
   
      for(var i = 0; i < this.randomSapphiresData.length; ++i) 
      {
         sapphire = new Sprite('sapphire', sapphireArtist, [ new CycleBehaviour(SPARKLE_DURATION), new BounceBehaviour(BOUNCE_DURATION_BASE + BOUNCE_DURATION_BASE * Math.random(), BOUNCE_HEIGHT_BASE +BOUNCE_HEIGHT_BASE * Math.random()) ]);
         sapphire.width = this.SAPPHIRE_CELLS_WIDTH;
         sapphire.height = this.SAPPHIRE_CELLS_HEIGHT;
         sapphire.value = 200;
         
         this.sapphires.push(sapphire);
      }
  },

  createPortalSprites: function()
  {
    var portal;

    for(var i=0; i<this.randomPortalData.length;++i)
    {
      portal = new Sprite('portal', new SpriteSheetArtist(this.spritesheet, this.portalCell));

      portal.width = 47;
      portal.height = 67;
      this.portals.push(portal);
    }
  },
  
  createRunnerSprite: function()
  {
    var RUNNER_LEFT = 50,
        RUNNER_HEIGHT = 53,
        STARTING_RUN_ANIMATION_RATE = 0,
        STARTING_RUNNER_TRACK = 1;

    this.runner = new Sprite('runner', new SpriteSheetArtist(this.spritesheet, this.runnerCellsRight), [this.runBehaviour, this.jumpBehaviour, this.collideBehaviour, this.fallBehaviour, this.runnerExplodeBehaviour]);
    this.runner.runAnimationRate = STARTING_RUN_ANIMATION_RATE;
    this.runner.track = STARTING_RUNNER_TRACK;
    this.runner.left = RUNNER_LEFT;
    this.runner.top = this.calculatePlatformTop(this.runner.track) - RUNNER_HEIGHT;
    this.runner.width = this.RUNNER_CELLS_WIDTH;
    this.runner.height = this.RUNNER_CELLS_HEIGHT;
    this.sprites.push(this.runner);
  },

  //Animation
  animate: function(now)
  {
    //Replace the time passed to animate by the browser
    //with our own game time
    now = game.timeSystem.calculateGameTime();
    if(game.paused)
    {
      setTimeout(function(){
        requestAnimationFrame(game.animate);
      }, game.PAUSED_CHECK_INTERVAL);
    }
    else
    {
      fps = game.calculateFps(now);

      if(fps < 30)
      {
        this.warning_fps++;
      }
      else
      {
        this.warning_fps = 0;
      }

      if(this.warning_fps > 3) //detects if fps is low more than 50 times
      {
        game.revealToast('Low FPS, check your device!', 2000);
      }

      game.draw(now);
      game.lastAnimationFrameTime = now;
      requestAnimationFrame(game.animate);
    }
  },

  calculateFps: function(now)
  {
    var fps = 1/(now - game.lastAnimationFrameTime) * 1000*
    this.timeRate;
    if(now - game.lastFpsUpdateTime > 1000)
    {
      game.lastFpsUpdateTime = now;
      game.fpsElement.innerHTML = fps.toFixed(0) + ' fps'; 
    }
    return fps;
  },

  initializeImages: function()
  {
    game.background.src = 'images/background.png';
    game.life.src = 'images/life.png';
    this.spritesheet.src = 'images/spritesheet-1.png';

    this.background.onload = function(e)
    {
      game.backgroundLoaded();
    };
  },

  backgroundLoaded: function()
  {
    var LOADING_SCREEN_TRANSITION_DURATION = 2000;
    this.fadeOutElements(this.loadingElement, LOADING_SCREEN_TRANSITION_DURATION);
    setTimeout(function(e){
      game.startGame();
      game.gameStarted = true;
    }, LOADING_SCREEN_TRANSITION_DURATION);
  },

     spritesheetLoaded: function () {
      var LOADING_SCREEN_TRANSITION_DURATION = 2000;

      this.graphicsReady = true;

      this.fadeOutElements(this.loadingElement, 
         LOADING_SCREEN_TRANSITION_DURATION);

      setTimeout ( function () {
         if (!game.gameStarted) {
            game.startGame();
         }
      }, LOADING_SCREEN_TRANSITION_DURATION);
   },

  loadingAnimationLoaded: function()
  {
    if(!this.gameStarted)
    {
      this.fadeInElements(this.loadingTitleElement);
    }
  },

  startGame: function()
  {
    if(game.mobile)
    {
       this.fadeInElements(game.mobileWelcomeToast);
    }
    else
    {   
      this.revealInitialToast();
    }

    this.revealGame();
    //this.createSprites();
    this.startMusic();
    this.timeSystem.start();
    this.gameStarted = true;

    window.requestAnimationFrame(game.animate);
  },

     startMusic: function()
   {
      var MUSIC_DELAY = 1000;

      setTimeout(function()
      {
         if(game.musicCheckboxElement.checked)
         {
            game.musicElement.play();
         }
         game.pollMusic();
      }, MUSIC_DELAY);
   },

   pollMusic: function()
   {
      var POLL_INTERVAL = 500,
      SOUNDTRACK_LENGTH = 132,
      timerID;

      timerID = setInterval(function()
      {
         if(game.musicElement.currentTime > SOUNDTRACK_LENGTH)
         {
            clearInterval(timerID); //stop polling
            game.restartMusic();
         }
      }, POLL_INTERVAL);
   },

   restartMusic: function(){
   game.musicElement.pause();
   game.musicElement.currentTime = 0;
   game.startMusic();
  },

  createAudioChannels: function()
   {
      var channel;

      for(var i=0; i < this.audioChannels.length; ++i)
      {
         channel = this.audioChannels[i];
         if(i !== 0)
         {
            channel.audio = document.createElement('audio');
            channel.audio.addEventListener('loaddata',
               this.soundLoaded, false);
            channel.audio.src = this.audioSprites.currentSrc;
         }
         channel.audio.autobuffer = true;
      }
   },

   soundLoaded: function()
   {
      game.audioSpriteCountdown--;
      if(game.audioSpriteCountdown === 0)
      {
         if(!game.gameStarted)
         {
            game.startGame();
         }
      }
   },

 playSound: function(sound)
   {
      var channel,audio;
      if(this.soundOn)
      {
         channel = this.getFirstAvailableAudioChannel();

         if(!channel)
         {
            if(console)
            {
               console.warn('All audio channels are busy. Cant play sound');
            }
         }
         else
         {
            var audio = channel.audio;
            audio.volume = sound.volume;

            this.seekAudio(sound, audio);
            this.playAudio(audio, channel);

             setTimeout(function () {
               channel.playing = false;
               game.seekAudio(sound, audio);
            }, sound.duration);
         }
      }
   },

    getFirstAvailableAudioChannel: function()
   {
      for(var i =0; i < this.audioChannels.length; ++i)
      {
         if(!this.audioChannels[i].playing)
         {
            return this.audioChannels[i];
         }
      }
      return null;
   },

   seekAudio: function(sound, audio)
   {
      try
      {
         audio.pause();
         audio.currentTime = sound.position;
      }
      catch(e)
      {
         console.error('Cannot play audio');
      }
   },

   playAudio: function(audio, channel)
   {
      try
      {
         audio.play();
         channel.playing = true;
      }
      catch(e)
      {
         if(console)
         {
            console.error('Cannot play audio');
         }
      }
   },


  setTimeRate: function(rate)
  {
    this.timeRate = rate;
    this.timeSystem.setTransducer(function(now)
    {
      return now * game.timeRate;
    });
  },

  revealGame: function()
  {
    var DIM_CONTROLS_DELAY = 5000;
    this.revealTopChromeDimmed();
    this.revealCanvas();
    this.revealBottomChrome();

    setTimeout(function() {
      game.dimControls();
      game.revealTopChrome();
    }, DIM_CONTROLS_DELAY);
  },

  revealTopChromeDimmed: function()
  {
    var DIM = 0.25;
    this.fpsElement.style.display = 'block';
    setTimeout(function(){
      game.fpsElement.style.opacity = 'block';
    }, this.SHORT_DELAY);
  },

  revealCanvas: function()
  {
    this.fadeInElements(this.canvas);
  },

  revealBottomChrome: function()
  {
    this.fadeInElements(this.soundAndMusicElement, this.instructionElement, this.copyrightElement);
  },

  dimControls: function()
  {
    var FINAL_OPACITY = 0.5;
    game.instructionElement.style.opacity = FINAL_OPACITY;
    game.soundAndMusicElement.style.opacity = FINAL_OPACITY;
  },

  revealTopChrome: function()
  {
    this.fadeInElements(this.fpsElement);
  },

  revealInitialToast: function()
  {
    var INITIAL_TOAST_DELAY = 1500,
        INITIAL_TOAST_DURATION = 3000;
    setTimeout(function(){
      game.revealToast('Collect the coins !', INITIAL_TOAST_DURATION);
    }, INITIAL_TOAST_DELAY);
    setTimeout(function(){
      game.revealToast('Avoid falling rocks !', INITIAL_TOAST_DURATION);
    }, INITIAL_TOAST_DELAY * 2);
  },

  addScore: function(value)
  {
    game.currentScore = game.currentScore + 100;
  },

  resetScore: function()
  {
    game.currentScore = 0;
  },

  resetPlayerLife: function()
  {
    game.playerLife = 3;
  },

  draw: function(now)
  {
    game.setPlatformVelocity();
    game.setOffsets(now);
    game.drawBackground();
    this.updateSprites(now);
    this.drawSprites();
    this.drawPlayerLife();
    this.drawInformation();
  },

  resetLevel: function()
  {
    this.currentLevel = 1;
    this.currentTimeRate = 1;
    this.setTimeRate(1.0);
    this.currentPlatformNum = this.INITIAL_PLATFORM_NUM;
    this.currentPlatformGap = this.INITIAL_PLATFORM_GAP;
    this.rockSpawnRate = this.INITIAL_ROCK_SPAWN_RATE;
    this.coinSpawnRate = this.INITIAL_COIN_SPAWN_RATE;
  },

  resetPlayerLife: function()
  {
    game.playerLife = 3;
  },

  updateSprites: function(now)
  {
    var sprite;
    for(var i=0; i<this.sprites.length; i++)
    {
      sprite = this.sprites[i];
      if(sprite.visible && this.isSpriteInView(sprite))
      {
        //this.context.translate(-sprite.hOffset, 0);
        sprite.update(now, this.fps, this.context, this.lastAnimationFrameTime);
        //this.context.translate(sprite.hOffset, 0);
      }
    }
  },

   detectMobile: function()
   {
      game.mobile = 'ontouchstart' in window;
   },

   fitScreen: function()
   {
      var arenaSize = game.calculateArenaSize(game.getViewportSize());
      game.resizeElementsToFitScreen(arenaSize.width, arenaSize.height);
   },

  getViewportSize: function()
   {
      return { 
         width: Math.max(document.documentElement.clientWidth || window.innerWidth || 0), 
         height: Math.max(document.documentElement.clientHeight || window.innerHeight || 0)};
   },

    drawMobileInstructions: function()
   {
      var cw = this.canvas.width,
          ch = this.canvas.height,
          TOP_LINE_OFFSET = 115,
          LINE_HEIGHT = 40;

      this.context.save();
      this.initializeContextForMobileInstruction();
      this.drawMobileDivider(cw, ch);
      this.drawMobileInstructionsLeft(cw, ch, TOP_LINE_OFFSET, LINE_HEIGHT);
      this.drawMobileInstructionsRight(cw, ch, TOP_LINE_OFFSET, LINE_HEIGHT);
      this.context.restore();
   },

   initializeContextForMobileInstruction: function()
   {
      this.context.textAlign = 'center';
      this.context.textBaseLine = 'middle';
      this.context.font = '26px fantasy';
      this.context.shadowBlur = 2;
      this.context.shadowOffsetX = 2;
      this.context.shadowOffsetY = 2;
      this.context.shadowColor = 'black';
      this.context.fillStyle = 'yellow';
      this.context.strokeStyle = 'yellow';
   },

    drawMobileDivider: function(cw, ch)
   {
      this.context.beginPath();
      this.context.moveTo(cw/2, 0);
      this.context.lineTo(cw/2, ch);
      this.context.stoke();
   },

   drawMobileInstructionsLeft: function(cw, ch, topLineOffset, lineHeight)
   {
      this.context.font = '32px fantasy';
      this.context.fillText('Tap on this side to:', cw/4, ch/2-topLineOffset);
      this.context.fillStyle = 'white';
      this.context.font = 'italic 26px fantasy';
      this.context.fillText('Turn around when running right', cw/4, ch/2 - topLineOffset + 2 * lineHeight);
      this.context.fillText('Jump when running left', cw/4,ch/2 - topLineOffset + 3 * lineHeight);
   },

  drawMobileInstructionsRight: function(cw, ch, topLineOffset, lineHeight)
   {
      this.context.font = '32px fantasy';
      this.context.fillText('Tap on this side to:', 3*cw/4, ch/2-topLineOffset);
      this.context.fillStyle = 'white';
      this.context.font = 'italic 26px fantasy';
      this.context.fillText('Turn around when running left', 3*cw/4,
         ch/2 - topLineOffset + 2 * lineHeight);
      this.context.fillText('Jump when running right', 3*cw/4,
         ch/2 - topLineOffset + 3 * lineHeight);
      this.context.fillText('Start running right', 3*cw/4,
         ch/2 - topLineOffset + 5 * lineHeight);
   },

  revealMobileStartToast: function(){
   game.fadeInElements(game.mobileStartToast);
   this.mobileInstructionsVisible = true;
  },

   calculateArenaSize: function(viewportSize)
   {
      var DESKTOP_ARENA_WIDTH = 800,
          DESKTOP_ARENA_HEIGHT = 520,
          arenaHeight,
          arenaWidth;
      //maintain aspect ratio
      arenaHeight = viewportSize.width*(DESKTOP_ARENA_HEIGHT/DESKTOP_ARENA_WIDTH);
      if(arenaHeight < viewportSize.height) //Height fits
      {
         arenaWidth = viewportSize.width;
      }
      else //Height does not fit
      {
         arenaHeight = viewportSize.height;
         arenaWidth = arenaHeight*(DESKTOP_ARENA_WIDTH/DESKTOP_ARENA_HEIGHT);
      }
      if(arenaWidth > DESKTOP_ARENA_WIDTH)
      {
         arenaWidth = DESKTOP_ARENA_WIDTH;
      }
      if(arenaHeight > DESKTOP_ARENA_HEIGHT)
      {
         arenaHeight = DESKTOP_ARENA_HEIGHT;
      }
      return { 
         width:  arenaWidth, 
         height: arenaHeight 
      };
   },

   resizeElementsToFitScreen: function(arenaWidth, arenaHeight)
   {
      game.resizeElement(document.getElementById('arena'), arenaWidth, arenaHeight);
      game.resizeElement(game.mobileWelcomeToast, arenaWidth, arenaHeight);
      game.resizeElement(game.mobileStartToast, arenaWidth, arenaHeight);
   },

   addTouchEventHandlers: function () {
   game.canvas.addEventListener( 'touchstart', game.touchStart);      
   game.canvas.addEventListener( 'touchend', game.touchEnd );   
  },

  touchStart: function (e) {      
    if (game.playing) {         
        // Prevent players from inadvertently dragging the game canvas         
      e.preventDefault();       
      }   
    },

    touchEnd: function (e) {      
    var x = e.changedTouches[0].pageX;      
    if (game.playing) {         
        if (x < game.canvas.width/2) {            
             game.processLeftTap();         
         }         
         else if (x > game.canvas.width/2) {                       
          game.processRightTap();         
         }         
        // Prevent players from double         
       // tapping to zoom into the canvas         
        e.preventDefault();       
    }   
  },

  processRightTap: function () 
  {      
   if (game.runner.direction === game.LEFT || game.bgVelocity === 0) 
   {         
      game.turnRight();
   }      
   else 
   {        
       game.runner.jump();      
   }
  },

  processLeftTap: function () 
  {      
    if (game.runner.direction === game.RIGHT) 
    {               
      game.turnLeft();      
    }      
    else 
    {         
      game.runner.jump();      
    }   
  },

  resizeElement: function(element, w, h)
   {
      //element.style.width = w + 'px';
      //element.style.height = h + 'px';
   },

  drawSprites: function()
  {
    var sprite;
    for(var i=0; i<this.sprites.length; i++)
    {
      sprite = this.sprites[i];
      if(sprite.visible && this.isSpriteInView(sprite))
      {
        this.context.translate(-sprite.hOffset, 0);
        sprite.draw(this.context);
        this.context.translate(sprite.hOffset, 0);
      }
    }
  },

  drawInformation: function()
  {
    this.context.font = "20px Comic Sans MS";
    this.context.fillStyle = "red";
    this.context.textAlign = "left";
    this.context.fillText("Life: ", 10, 26);
    for(var i=0; i < this.playerLife; i++)
    {
      this.context.drawImage(game.life, this.lifeData[i].left, this.lifeData[i].top);
    }

    this.context.fillStyle = "orange";
    this.context.fillText("Level: " + this.currentLevel, 200, 26);

    this.context.fillStyle = "yellow";
    this.context.fillText("Score: " + this.currentScore, 350, 26);

    this.context.fillStyle = "red";
    this.context.fillText("High Score: " + this.highScoreThisSession, 620, 26);

    /*
    this.context.font = "15px Comic Sans MS";
    this.context.fillStyle = 'yellow';
    this.context.fillText("Data Checks: ", 650, this.canvas.height - 95);
    this.context.fillText("Time Rate: " + this.timeRate, 650, this.canvas.height - 80);
    this.context.fillText("Platform Num: " + this.currentPlatformNum, 650, this.canvas.height - 65);
    this.context.fillText("Platform Gap: " + this.currentPlatformGap, 650, this.canvas.height - 50);
    this.context.fillText("Rock Spawn Rate: " + this.rockSpawnRate, 650, this.canvas.height - 35);
    this.context.fillText("Coin Spawn Rate: " + this.coinSpawnRate, 650, this.canvas.height - 20);
    */
  },

  drawCurrentLevel: function()
  {
    this.context.font = "20px Comic Sans MS";
    this.context.fillStyle = "orange";
    this.context.textAlign = "left";
    this.context.fillText("Level: " + this.currentLevel, 200, 26);
    this.context.fillText("Platform Num: " + this.currentPlatformNum, 500, 26);
  },

  drawPlayerLife: function()
  {
    this.context.font = "20px Comic Sans MS";
    this.context.fillStyle = "red";
    this.context.textAlign = "left";
    this.context.fillText("Life: ", 10, 26);
    for(var i=0; i < this.playerLife; i++)
    {
      this.context.drawImage(game.life, this.lifeData[i].left, this.lifeData[i].top);
    }
  },

  isSpriteInView: function(sprite)
  {
      return sprite.left+sprite.width > sprite.hOffset && sprite.left < sprite.hOffset + this.canvas.width;
  },

  drawBackground: function()
  {
    game.context.translate(-game.backgroundOffset, 0);
    game.context.drawImage(game.background, 0, 0);
    game.context.drawImage(game.background, game.background.width, 0);
    game.context.translate(game.backgroundOffset, 0);
  },

  drawRunner: function()
  {
    game.context.drawImage(game.runner, game.RUNNER_LEFT, game.calculatePlatformTop(game.STARTING_RUNNER_TRACK) - game.runner.height);
  },

  /*
  togglePausedStateOfAllBehaviours: function (now) {
      
      var behaviour;
   
      for (var i=0; i < this.sprites.length; ++i) {
         sprite = this.sprites[i];

         for (var j=0; j < sprite.behaviours.length; ++j) {
            behaviour = sprite.behaviours[j];

            if (this.paused) {
               if (behaviour.pause) {
                  behaviour.pause(sprite, now);
               }
            }
            else {
               if (behaviour.unpause) {
                  behaviour.unpause(sprite, now);
               }
            }
         }
      }
   },
  */
  calculatePlatformTop: function(track)
  {
    if(track === 1)
    {
      return game.TRACK_1_BASELINE;
    }
    else if(track === 2)
    {
      return game.TRACK_2_BASELINE;
    }
    else if(track === 3)
    {
      return game.TRACK_3_BASELINE;
    }
  },

  setBackgroundOffset: function(now)
  {
    game.backgroundOffset += game.bgVelocity*(now - game.lastAnimationFrameTime)/1000;
    if(game.backgroundOffset < 0 || game.backgroundOffset > game.background.width)
    {
      game.backgroundOffset = 0;
    }
  },

  turnLeft: function()
  {
    game.bgVelocity = -game.BACKGROUND_VELOCITY;
    this.runner.runAnimationRate = this.RUN_ANIMATION_RATE;
    this.runner.artist.cells = this.runnerCellsLeft;
  },

  turnRight: function()
  {
    game.bgVelocity = game.BACKGROUND_VELOCITY;
    this.runner.runAnimationRate = this.RUN_ANIMATION_RATE;
    this.runner.artist.cells = this.runnerCellsRight;
  },

  setPlatformVelocity: function()
  {
    game.platformVelocity = game.bgVelocity * game.PLATFORM_VELOCITY_MULTIPLER;
  },

  setPlatformOffset: function(now)
  {
    game.platformOffset += game.platformVelocity*(now - game.lastAnimationFrameTime)/1000;
    //console.log(game.platformOffset);
    if(game.platformOffset > 2*game.background.width)
    {
      game.turnLeft();
    }
    else if(game.platformOffset < 0)
    {
      game.turnRight();
    }
  },

  setOffsets: function(now)
  {
    game.setBackgroundOffset(now);
    game.setSpriteOffsets(now);
  },

  setSpriteOffsets: function (now) {
      var sprite;
   
      // In step with platforms
      this.spriteOffset +=
         this.platformVelocity * (now - this.lastAnimationFrameTime) / 1000;

      for (var i=0; i < this.sprites.length; ++i) {
         sprite = this.sprites[i];

         if ('runner' !== sprite.type) {
            sprite.hOffset = this.spriteOffset; 
         }
      }
   },

  togglePaused: function() {
    var now = this.timeSystem.calculateGameTime();
    this.paused = !this.paused;
    if(this.paused)
    {
      this.pauseStartTime = now;
    }
    else
    {
      this.lastAnimationFrameTime += (now - this.pauseStartTime);
    }

      if(this.musicOn)
      {
         if(this.paused)
         {
            this.musicElement.pause();
         }
         else
         {
            this.musicElement.play();
         }
      }
  },

  revealToast: function(text, duration)
  {
    var DEFAULT_TOAST_DURATION = 1000;
    duration = duration || DEFAULT_TOAST_DURATION;
    this.startToastTransition(text, duration);
    setTimeout(function(e){
      game.hideToast();
    }, duration);
  },

  startToastTransition: function(text, duration)
  {
    this.toastElement.innerHTML = text;
    this.fadeInElements(this.toastElement);
  },

  hideToast: function()
  {
    var TOAST_TRANSITION_DURATION = 500;
    this.fadeOutElements(this.toastElement, TOAST_TRANSITION_DURATION);
  },

  fadeInElements: function()
  {
    if(!game.mobile){
    var args = arguments;
    for(var i=0; i < args.length; ++i)
    {
      args[i].style.display = 'block';
    }
    setTimeout(function(e){
      for(var i=0; i < args.length; ++i)
      {
        args[i].style.opacity = game.OPAQUE;
      }
    }, this.SHORT_DELAY);}
  },

  fadeOutElements: function ()
  {
    var args = arguments,
      fadeDuration = args[args.length-1];
    for(var i=0; i < args.length-1; ++i)
    {
      //console.log(args[i]);
      args[i].style.opacity = game.TRANSPARENT;
    }

    setTimeout(function () {
    for(var i=0; i < args.length-1; ++i)
    {
      args[i].style.display = 'none';
    }
    }, fadeDuration);
  },

  //Effects
   explode: function (sprite) 
   {
      if ( ! sprite.exploding) {
         if (sprite.runAnimationRate === 0) {
            sprite.runAnimationRate = this.RUN_ANIMATION_RATE;
         }

         sprite.exploding = true;
         game.playSound(game.explosionSound);
      }
   },

  shake: function () 
  {
      var SHAKE_INTERVAL = 80, // milliseconds
          v = game.BACKGROUND_VELOCITY*1.5,
          ov = game.bgVelocity; // ov means original velocity
   
      game.bgVelocity = -v;

      setTimeout( function (e) {
       game.bgVelocity = v;
       setTimeout( function (e) {
          game.bgVelocity = -v;
          setTimeout( function (e) {
             game.bgVelocity = v;
             setTimeout( function (e) {
                game.bgVelocity = -v;
                setTimeout( function (e) {
                   game.bgVelocity = v;
                   setTimeout( function (e) {
                      game.bgVelocity = -v;
                      setTimeout( function (e) {
                         game.bgVelocity = v;
                         setTimeout( function (e) {
                            game.bgVelocity = -v;
                            setTimeout( function (e) {
                               game.bgVelocity = v;
                               setTimeout( function (e) {
                                  game.bgVelocity = -v;
                                  setTimeout( function (e) {
                                     game.bgVelocity = v;
                                     setTimeout( function (e) {
                                        game.bgVelocity = ov;
                                     }, SHAKE_INTERVAL);
                                  }, SHAKE_INTERVAL);
                               }, SHAKE_INTERVAL);
                            }, SHAKE_INTERVAL);
                         }, SHAKE_INTERVAL);
                      }, SHAKE_INTERVAL);
                   }, SHAKE_INTERVAL);
                }, SHAKE_INTERVAL);
             }, SHAKE_INTERVAL);
          }, SHAKE_INTERVAL);
       }, SHAKE_INTERVAL);
     }, SHAKE_INTERVAL);
  },

   levelProceed: function()
   {
      var TRANSITION_DURATION = 2000;
      this.startLifeTransition(game.RUNNER_EXPOSION_DURATION);
      setTimeout(function(){
        game.endLifeTransition();
      }, TRANSITION_DURATION);

      this.scoreBeforeThisLevel = this.currentScore;

      this.currentLevel++;
      this.currentPlatformNum += 5;

      this.currentTimeRate = 1.0 + (this.currentLevel - 1) * 0.05;
      game.setTimeRate(1.0 + (this.currentLevel - 1) * 0.05);

      if(this.rockSpawnRate < 70)
      {
        this.rockSpawnRate += 5;
      }

      if(this.coinSpawnRate > 30)
      {
        this.coinSpawnRate -= 5;
      }

      if(this.currentPlatformGap < 270)
      {
        this.currentPlatformGap += 5;
      }

      setTimeout(function(){
        game.resetSprites();
        game.createSprites();
      }, 2000);
   },

   loseLife: function()
   {
      var TRANSITION_DURATION = 3000;
      this.playerLife--;
      this.startLifeTransition(game.RUNNER_EXPOSION_DURATION);
      setTimeout(function(){
        game.endLifeTransition();
      }, TRANSITION_DURATION);

      if(this.currentScore > this.highScoreThisSession)
      {
        this.highScoreThisSession = this.currentScore;
      }

      this.currentScore = this.scoreBeforeThisLevel;

      if(this.playerLife === 0)
      {
        game.revealToast('Game Over', 1500);
        setTimeout(function(){
          game.resetPlayerLife();
        }, 2000);

        game.resetScore();
        game.resetLevel();
      }
   },

   startLifeTransition: function(slowMotionDelay)
   {
      var CANVAS_TRANSITION_OPACITY = 0.05,
        SLOW_MOTION_RATE = 0.1;
      this.canvas.style.opacity = CANVAS_TRANSITION_OPACITY;
      this.playing = false;
      setTimeout(function(){
        game.setTimeRate(SLOW_MOTION_RATE);
        game.runner.visible = false;
      }, slowMotionDelay);
      this.setTimeRate(this.currentTimeRate);
   },

   endLifeTransition: function()
   {
      var TIME_RESET_DELAY = 1000,
          RUN_DELAY = 500;
      game.reset();
      setTimeout(function(){
        game.setTimeRate(1.0);
        setTimeout(function(){
          game.runner.runAnimationRate = 0;
          game.playing = true;
        }, RUN_DELAY);
      }, TIME_RESET_DELAY);
   },

   reset: function()
   {
     game.resetOffsets();
     game.resetRunner();
     game.makeAllSpritesVisible();
     game.canvas.style.opacity = 1.0;
   },

   resetRunner: function()
   {
      this.runner.left = game.RUNNER_LEFT;
      this.runner.track = 1;
      this.runner.hOffset = 0;
      this.runner.visible = true;
      this.runner.exploding = false;
      this.runner.jumping = false;
      this.runner.top = this.calculatePlatformTop(1) - this.runner.height;
      this.runner.artist.cells = this.runnerCellsRight;
      this.runner.artist.cellIndex = 0;
   },

   resetOffsets: function()
   {
      this.bgVelocity = 0;
      this.backgroundOffset = 0;
      this.platformOffset = 0;
      this.spriteOffset = 0;
   },

   makeAllSpritesVisible: function()
   {
      for(var i=0; i < game.sprites.length; ++i)
      {
        game.sprites[i].visible = true;
      }
   }

};

window.addEventListener('keydown', function(e){
  var key = e.keyCode;
  if(!game.playing || game.runner.exploding)
  {
    return;
  }
  if(key === game.KEY_D || key === game.KEY_LEFT) 
  {
    game.turnLeft();
  }
  else if(key === game.KEY_K || key === game.KEY_RIGHT) 
  {
    game.turnRight();
  }
  else if(key === game.KEY_P)
  {
    game.togglePaused();
  }
  else if(key === 74)
  {
    game.runner.jump();
  }
});

window.addEventListener('blur', function(e)
{
  game.windowHasFocus = false;
  if(!game.paused)
  {
    game.togglePaused(); //Pause the game
}});

window.addEventListener('focus', function(e)
{
  var originalFont = game.toastElement.style.fontSize,
  DIGIT_DISPLAY_DURATION = 1000;

  game.windowHasFocus = true;
  game.countDownInProgress = true;
  if(game.paused)
  {
    game.toastElement.style.font = '128px fantasy';
    if(game.windowHasFocus && game.countDownInProgress)
    {
      game.revealToast('3', DIGIT_DISPLAY_DURATION);
    }
    setTimeout(function (e){
      if(game.windowHasFocus && game.countDownInProgress)
      {
        game.revealToast('2', DIGIT_DISPLAY_DURATION);
      }
      setTimeout(function (e){
      if(game.windowHasFocus && game.countDownInProgress)
      {
        game.revealToast('1', DIGIT_DISPLAY_DURATION);
      }
        setTimeout(function (e){
      if(game.windowHasFocus && game.countDownInProgress)
      {
        game.togglePaused();
      }
      if(game.windowHasFocus && game.countDownInProgress)
      {
        game.toastElement.style.fontSize = originalFont;
      }
      game.countdownInProgress = false;
      }, DIGIT_DISPLAY_DURATION);
      }, DIGIT_DISPLAY_DURATION);
    },DIGIT_DISPLAY_DURATION);
  }
});

var game = new Game();

game.musicCheckboxElement.addEventListener('change', function (e){
   game.musicOn = game.musicCheckboxElement.checked;
   if(game.musicOn)
   {
      game.musicElement.play();
   }
   else
   {
      game.musicElement.pause();
   }
});

game.soundCheckboxElement.addEventListener('change', function (e){
   game.soundOn = game.soundCheckboxElement.checked;
});

game.welcomeStartLink.addEventListener('click',function(e)
   {
      var FADE_DURATION = 1000;
      game.playSound(game.coinSound);
      game.fadeOutElements(game.mobileWelcomeToast, FADE_DURATION);
      game.playing = true;
   });

game.mobileStartLink.addEventListener('click', function(e)
    {
      var FADE_DURATION = 1000;
      game.fadeOutElements(game.mobileStartToast, FADE_DURATION);
      game.mobileInstructionsVisible = false;
      game.playSound(game.coinSound);
      game.playing = true;
   });

game.showHowLink.addEventListener('click', function(e)
{
   var FADE_DURATION = 1000;
   game.fadeOutElements(game.mobileWelcomeToast, FADE_DURATION);
   game.drawMobileInstructions();
   game.revealMobileStartToast();
   game.mobileInstructionsVisible = true;
});

game.initializeImages();
game.createSprites();
game.createAudioChannels();
game.detectMobile();

if(game.mobile)
{
   //mobile specific stuff - touch handler etc
   game.instructionsElement = document.getElementById('mobile-instructions');
   game.addTouchEventHandlers();
}

game.fitScreen();
window.addEventListener("resize", game.fitScreen());
window.addEventListener("orientationchange", game.fitScreen());