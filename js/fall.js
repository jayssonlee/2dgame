var RockFallBehaviour = function()
{
	this.GRAVITY_FORCE = 9.81;
	this.falling = false;
	this.timer = new AnimationTimer(3500, 
		AnimationTimer.makeEaseLinearEasingFunction());
	this.paused = false;
};

RockFallBehaviour.prototype = 
{
	pause: function(now)
	{
		if(!this.timer.isPaused())
		{
			this.timer.pause(now);
		}
		this.paused = true;
	},

	unpause: function(now)
	{
		if(this.timer.isPaused())
		{
			this.timer.unpause(now);
		}
		this.paused = false;
	},

	startFalling: function(sprite, now)
	{
		this.falling = true;
		this.timer.start(now);
	},

	resetTimer: function(now)
	{
		this.timer.stop(now);
		this.timer.reset(now);
		this.timer.start(now);
	},

	adjustVerticalPostion: function(sprite, elapsed, now)
	{
		sprite.top = sprite.top + this.timer.getElapsedTime(now) / 150;

		if(this.timer.getElapsedTime(now) + 1000*Math.random() > 3000)
		{
			sprite.top = -50;
			this.resetTimer(now);
		}
	},

	execute: function(sprite, now, fps, context, lastAnimationFrameTime)
	{
		var elapsed;

		if(!this.falling)
		{
			this.startFalling(sprite, now);
		}
		else
		{
			elapsed = this.timer.getElapsedTime(now);
			if(this.timer.isExpired(now))
			{
				this.resetTimer(now);
				return;
			}
			this.adjustVerticalPostion(sprite, elapsed, now);
		}
	}
};